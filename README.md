# CPSC 223 ULA Scenario

Please send your responses to the two prompts in the office hours
scenario below.

## First problem
A student comes to you for help with their implementation of an array-based
list of points.  The file ``plist.c`` implements the abstract data type
defined in ``plist.h`` (there is a similar pair of files ``point.c``
and ``point.h`` for the point ADT, but those are instuctor-provided files
and aren't relevant for this exercise).  There is also a collection of unit
tests for the list of points ADT in ``plist_unit.c``.  The ``makefile``
creates an executable ``Unit`` that you can use to run each of the unit tests.
For example, ``./Unit 1`` runs the first test, which checks that the size
function works properly via the ``testSize`` function.

```C
void testSize()
{
  plist *list = plist_create();

  point p;
  p.x = 1;
  p.y = 2;

  for (int i = 0; i < 1000000; i++)
    {
      if (plist_size(list) != i)
	{
	  printf("FAILED: size is %d; should be %d\n", plist_size(list), i);
	  return;
	}
      plist_add_end(list, &p);
    }

  printf("PASSED\n");
  
  // don't want to test destroy here
}
```

The student says "my code fails on the first unit test, which says it tests
the size function, but my size function just returns ``plist->size``; how
can that be wrong?".

You take a quick glance through the student's code and notice (perhaps
among other things) that they don't reset capacity in ``plist_add_end``
in the case when the array needs to be resized.

```C
bool plist_add_end(plist *l, const point *p)
{
  if (l == NULL || p == NULL)
    {
      return false;
    }

  if (l->capacity == l->size)
    {
      int larger_capacity = (l->capacity * 2 > PLIST_INITIAL_CAPACITY
			     ? l->capacity * 2
			     : PLIST_INITIAL_CAPACITY);
      point *larger = realloc(l->items, larger_capacity);
      l->items = larger;
    }

  if (l->capacity > l->size)
    {
      l->items[l->size] = *p;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}
```

What do you say to the student?

## Second problem

You add

```C
l->capacity = larger_capacity
```

to the appropriate place.  Now ``./Unit 1`` crashes with the message
``realloc(): invalid next size``.  The student is upset: "that made it
worse!  What am I supposed to do now?"

What do you say to the student?
