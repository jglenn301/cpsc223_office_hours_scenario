CC = gcc
CFLAGS = -Wall -std=c99 -pedantic -g3

Unit: plist_unit.o plist.o point.o
	${CC} -o $@ ${CFLAGS} $^ -lm

