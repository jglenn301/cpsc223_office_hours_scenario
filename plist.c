#include <stdlib.h>
#include <string.h>

#include "point.h"
#include "plist.h"

struct plist
{
  int capacity;
  int size;
  point *items;
};

#define PLIST_INITIAL_CAPACITY 2
#define PLIST_LEFT_CHILD(i) 2*(i) + 1
#define PLIST_RIGHT_CHILD(i) 2*(i) + 2

plist *plist_create()
{
  plist *l = malloc(sizeof(plist));
  if (l != NULL)
    {
      l->items = malloc(sizeof(point) * PLIST_INITIAL_CAPACITY);
      l->size = 0;
      l->capacity = (l->items != NULL) ? PLIST_INITIAL_CAPACITY : 0;
    }
  return l;
}

void plist_destroy(plist *l)
{
  if (l == NULL)
    {
      return;
    }
  
  free(l->items);
  l->size = 0;
  l->items = NULL;
  l->capacity = 0;
  free(l);
}

int plist_size(const plist *l)
{
  if (l == NULL)
    {
      return 0;
    }

  return l->size;
}

bool plist_add_end(plist *l, const point *p)
{
  if (l == NULL || p == NULL)
    {
      return false;
    }

  if (l->capacity == l->size)
    {
      int larger_capacity = (l->capacity * 2 > PLIST_INITIAL_CAPACITY
			     ? l->capacity * 2
			     : PLIST_INITIAL_CAPACITY);
      point *larger = realloc(l->items, larger_capacity);
      l->items = larger;
    }

  if (l->capacity > l->size)
    {
      l->items[l->size] = *p;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}

void plist_get(const plist *l, int i, point *p)
{
  if (l == NULL || p == NULL)
    {
      return;
    }

  if (i < l->size && i >= 0)
    {
      *p = l->items[i];
    }
}

bool plist_contains(const plist *l, const point *p)
{
  int i = 0;
  while (i < l->size && (l->items[i].x != p->x || l->items[i].y != p->y))
    {
      i++;
    }
  return (i != l->size);
}

void plist_fprintf(FILE *stream, const char *fmt, const plist *l)
{
  if (stream == NULL || fmt == NULL || l == NULL)
    {
      return;
    }

  int len = strlen(fmt);
  char copy_fmt[len + 1];
  strcpy(copy_fmt, fmt);
  bool newline = false;
  if (fmt[len - 1] == '\n')
    {
      newline = true;
      copy_fmt[len - 1] = '\0';
    }
  fprintf(stream, "[");
  for (int i = 0; i < l->size; i++)
    {
      if (i > 0)
	{
	  fprintf(stream, ", ");
	}
      point_fprintf(stream, copy_fmt, &l->items[i]);
    }
  fprintf(stream, "]");
  if (newline)
    {
      fprintf(stream, "\n");
    }
}

void plist_build_heap(plist *l, int (*compare)(const point *, const point *));
void plist_reheap_down(plist *l, int i, int n, int (*compare)(const point *, const point *));
void plist_swap(plist *l, int i, int j);

int plist_adapt_compare(const void *, const void *, void *);
int plist_call_compare(const void *, const void *);

struct function_holder
{
  int (*function)(const point*, const point*);
};

int (*plist_compare)(const point*, const point*);

void plist_sort(plist *l, int (*compare)(const point *, const point *))
{
  plist_compare = compare;
  qsort(l->items, l->size, sizeof(point), plist_call_compare);
  //qsort(l->items, l->size, sizeof(point), (int(*)(const void *, const void *))compare);
  // need to define _GNU_SOURCE? (yes)
  //  struct function_holder compare_holder = { compare };
  //qsort_r(l->items, l->size, sizeof(point), plist_adapt_compare, &compare_holder);
  
  /*
  if (l == NULL || compare == NULL)
    {
      return;
    }

  plist_build_heap(l, compare);
  
  for (int n = l->size; n > 1; n--)
    {
      plist_swap(l, 0, n - 1);
      plist_reheap_down(l, 0, n - 1, compare);
    }
  */
}

int plist_call_compare(const void *p1, const void *p2)
{
  return plist_compare((const point *)p1, (const point *)p2);
}

int plist_adapt_compare(const void *p1, const void *p2, void *holder)
{
  const point *pt1 = p1;
  const point *pt2 = p2;
  struct function_holder *compare_holder = holder;

  return compare_holder->function(pt1, pt2);
}


void plist_build_heap(plist *l, int (*compare)(const point *, const point *))
{
  for (int i = l->size / 2; i >= 0; i--)
    {
      plist_reheap_down(l, i, l->size, compare);
    }
}

void plist_reheap_down(plist *l, int i, int n, int (*compare)(const point *, const point *))
{
  int left;
  while ((left = PLIST_LEFT_CHILD(i)) < n)
    {
      int right = left + 1;
      int largest_child = left;
      if (right < n && compare(&l->items[right], &l->items[left]) > 0)
	{
	  largest_child = right;
	}
      if (compare(&l->items[largest_child], &l->items[i]) > 0)
	{
	  plist_swap(l, i, largest_child);
	  i = largest_child;
	}
      else
	{
	  i = n;
	}
    }
}

void plist_swap(plist *l, int i, int j)
{
  if (i != j)
    {
      point temp = l->items[i];
      l->items[i] = l->items[j];
      l->items[j] = temp;
    }
}

