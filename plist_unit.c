#include <stdio.h>
#include <stdlib.h>

#include "point.h"
#include "plist.h"

int compare_points(const point *p1, const point *p2);

void testSize();
void testGet();
void testAddEndCopies();
void testGetReturnsCopy();
void testGetInvalidIndex();
void testContainsFound();
void testContainsNotFound();
void testPrint(FILE *stream, const char *fmt);
void testSort();
void testDestroy();
void testAddEfficiency();

int main(int argc, char **argv)
{
  if (argc == 1)
    {
      fprintf(stderr, "USAGE: %s test-number\n", argv[0]);
      return 1;
    }

  int test = atoi(argv[1]);
  switch (test)
    {
    case 1:
      testSize();
      break;

    case 2:
      testGet();
      break;

    case 3:
      testAddEndCopies();
      break;

    case 4:
      testGetReturnsCopy();
      break;

    case 5:
      testGetInvalidIndex();
      break;
      
    case 6:
      testContainsFound();
      break;
      
    case 7:
      testContainsNotFound();
      break;
      
    case 8:
      testPrint(stdout, "%.2f");
      break;
      
    case 9:
      testPrint(stdout, "%.2f\n");
      break;

    case 10:
      testPrint(stdout, "%.2f\n\n");
      break;
      
    case 11:
      testPrint(stderr, "%.2f\n");
      break;

    case 12:
      testSort();
      break;

    case 13:
      testDestroy();
      break;

    case 14:
      testAddEfficiency();
      break;

    default:
      fprintf(stderr, "%s: invalid test number %d\n", argv[0], test);
      return 1;
    }

  return 0;
}

int compare_points(const point *p1, const point *p2)
{
  // compare by distance to origin
  double d1 = (p1->x * p1->x) + (p1->y * p1->y);
  double d2 = (p2->x * p2->x) + (p2->y * p2->y);

  if (d1 < d2)
    {
      return -1;
    }
  else if (d1 > d2)
    {
      return 1;
    }
  else if (p1->x < p2->y)
    {
      return -1;
    }
  else if (p1->x > p2->y)
    {
      return 1;
    }
  else if (p1->y < p2->y)
    {
      return -1;
    }
  else if (p1->y > p2->y)
    {
      return 1;
    }
  else
    {
      return 0;
    }
}

void testSize()
{
  plist *list = plist_create();

  point p;
  p.x = 1;
  p.y = 2;

  for (int i = 0; i < 1000000; i++)
    {
      if (plist_size(list) != i)
	{
	  printf("FAILED: size is %d; should be %d\n", plist_size(list), i);
	  return;
	}
      plist_add_end(list, &p);
    }

  printf("PASSED\n");
  
  // don't want to test destroy here
}

void testGet()
{
  plist *list = plist_create();

  for (int i = 0; i < 10; i++)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(i * i + 0.25);
      plist_add_end(list, &p);
    };
      
  for (int i = 0; i < 10; i++)
    {
      point p;
      plist_get(list, i, &p);
      
      if (p.x != i + 0.5 || p.y != -(i * i + 0.25))
	{
	  printf("FAILED: got (%f, %f), not (%f, %f)\n",
		 p.x, p.y,
		 i + 0.5, -(i * i + 0.25));
	  return;
	}
    }

  printf("PASSED\n");
}

void testAddEndCopies()
{
  plist *list = plist_create();

  point p1, p2;
  p1.x = 4;
  p1.y = 6;
  p2.x = -1;
  p2.y = -4;

  plist_add_end(list, &p1);
  plist_add_end(list, &p2);

  p1.x = 99;
  p1.y = 99;
  p2.x = 99;
  p2.y = 99;

  point g1, g2;
  plist_get(list, 0, &g1);
  plist_get(list, 1, &g2);

  if (g1.x != 4 || g1.y != 6 || g2.x != -1 || g2.y != -4)
    {
      printf("FAILED\n");
      return;
    }
  else
    {
      printf("PASSED\n");
    }
}

void testGetReturnsCopy()
{
  // really don't see how this can fail
  plist *list = plist_create();

  point p1;
  p1.x = 4;
  p1.y = 6;

  plist_add_end(list, &p1);
  point g1;
  plist_get(list, 0, &g1);
  g1.x = 99;
  g1.y = 99;
  point g2;
  plist_get(list, 0, &g2);
  if (g2.x != 4 || g2.y != 6)
    {
      printf("FAILED\n");
      return;
    }
  else
    {
      printf("PASSED\n");
    }
}

void testGetInvalidIndex()
{
  plist *list = plist_create();

  for (int i = 0; i < 10; i++)
    {
      point p;
      p.x = 0;
      p.y = 0;

      plist_add_end(list, &p);
    }

  for (int i = 10; i < 100000; i++)
    {
      point q;
      q.x = 410;
      q.y = 443;
      
      plist_get(list, i, &q);
      if (q.x != 410 || q.y != 443)
	{
	  printf("FAILED\n");
	  return;
	}
    }
  
  for (int i = -1; i >= -100000; i--)
    {
      point q;
      q.x = 410;
      q.y = 443;
      
      plist_get(list, i, &q);
      if (q.x != 410 || q.y != 443)
	{
	  printf("FAILED\n");
	  return;
	}
    }

  printf("PASSED\n");
}

void testContainsFound()
{
  plist *list = plist_create();

  for (int i = 0; i < 10; i++)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(i * i + 0.25);
      plist_add_end(list, &p);
    };
      
  for (int i = 9; i >= 0; i--)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(i * i + 0.25);

      if (!plist_contains(list, &p))
	{
	  printf("FAILED: could not find (%f, %f) at index %d\n",
		 p.x, p.y, i);
	  return;
	}
    }
  printf("PASSED\n");
}

void testContainsNotFound()
{
  plist *list = plist_create();

  for (int i = 0; i < 10; i++)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(i * i + 0.25);
      plist_add_end(list, &p);
    };

  point q;
  q.x = 0.5;
  q.y = -1.25;

  if (plist_contains(list, &q))
    {
      printf("FAILED: found (%f, %f) which is not on the list\n",
	     q.x, q.y);
      return;
    }
  else
    {
      printf("PASSED\n");
    }
}

void testPrint(FILE *stream, const char *fmt)
{
  plist *list = plist_create();

  for (int i = 0; i < 3; i++)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(i * i + 0.25);
      plist_add_end(list, &p);
    };

  plist_fprintf(stream, fmt, list);
  fprintf(stream, "\n");
}

void testSort()
{
  plist *list = plist_create();
  int x = 13;
  int y = 42;
  
  for (int i = 0; i < 10; i++)
    {
      point p;
      p.x = x % 100;
      p.y = y % 100;

      plist_add_end(list, &p);

      x = ((x + 92) * 741) % 10000;
      y = ((y + 29) * 147) % 10000;
    }

  plist_sort(list, compare_points);

  for (int i = 0; i < 9; i++)
    {
      point p, q;
      plist_get(list, i, &p);
      plist_get(list, i + 1, &q);

      double d1 = p.x * p.x + p.y * p.y;
      double d2 = q.x * q.x + q.y * q.y;

      if (d1 > d2)
	{
	  printf("FAILED: (%f, %f) before (%f, %f)\n",
		 p.x, p.y, q.x, q.y);
	  return;
	}
    }
  printf("PASSED\n");
}

void testDestroy()
{
  plist *list = plist_create();

  for (int i = 0; i < 200000; i++)
    {
      point p;
      p.x = i + 0.5;
      p.y = -(2 * i + 0.25);
      plist_add_end(list, &p);
    };

  plist_destroy(list);

  printf("PASSED\n");
}

void testAddEfficiency()
{
  plist *list = plist_create();

  for (int i = 0; i < 200000; i++)
    {
      point p;
      p.x = 1;
      p.y = 1;
      int *i = malloc(sizeof(int));
      plist_add_end(list, &p);
      free(i);
    };

  plist_destroy(list);

  printf("PASSED\n");
}
